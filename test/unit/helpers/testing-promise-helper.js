export default class TestingPromiseHelper {

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.decider = { resolve, reject };
    });
  }

}
