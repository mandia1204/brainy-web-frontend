import Nomination from '../../../src/models/nomination';

describe('Nomination', () => {
  let someNomination;

  describe('ctor', () => {
    it('should copy attributes from object sent', () => {
      const baseNomination = {
        id: 7,
        nomineeName: 'John Smith',
        nomineeEmail: 'johnsmith@avantica.net',
        nominatorName: 'Will Rainbow',
        nominatorEmail: 'willrainbow@avantica.net',
        nominatorPosition: 'Software Engineer',
        description: 'He is a great guy to follow',
        competence: 'Passion',
        ackId: 6,
      };
      someNomination = new Nomination(baseNomination);
      expect(someNomination.id).toBe(7);
      expect(someNomination.nomineeName).toBe('John Smith');
      expect(someNomination.nomineeEmail).toBe('johnsmith@avantica.net');
      expect(someNomination.nominatorName).toBe('Will Rainbow');
      expect(someNomination.nominatorEmail).toBe('willrainbow@avantica.net');
      expect(someNomination.nominatorPosition).toBe('Software Engineer');
      expect(someNomination.description).toBe('He is a great guy to follow');
      expect(someNomination.competence).toBe('Passion');
      expect(someNomination.status).toBe('');
      expect(someNomination.ackId).toBe(6);
    });

    it('should use default value for name when nothing is sent', () => {
      someNomination = new Nomination();
      expect(someNomination.id).toBe(null);
      expect(someNomination.nomineeName).toBe('');
      expect(someNomination.nomineeEmail).toBe('');
      expect(someNomination.nominatorName).toBe('');
      expect(someNomination.nominatorEmail).toBe('');
      expect(someNomination.nominatorPosition).toBe('');
      expect(someNomination.description).toBe('');
      expect(someNomination.competence).toBe('');
      expect(someNomination.status).toBe('');
      expect(someNomination.ackId).toBe(null);
    });
  });
});
