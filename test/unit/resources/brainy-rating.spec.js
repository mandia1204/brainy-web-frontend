import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';

describe('BrainyRating', () => {
  let ratingComponent;

  beforeEach(() => {
    ratingComponent = StageComponent
      .withResources('resources/custom-elements/brainy-rating.html')
      .inView(`<brainy-rating
          settings.bind="{rating: skill,
            ratingName: 'someLevel'}"></brainy-rating>`)
      .boundTo({ skill: { someLevel: 4 } });
  });

  afterEach(() => {
    ratingComponent.dispose();
  });

  xit('should render 4 stars when value set is 4', (done) => {
    ratingComponent.create(bootstrap)
      .then(() => {
        const stars = document.querySelectorAll('li');
        expect(stars.length).toBe(4);
        done();
      });
  });
});
