import { bindable, inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { TagService } from '../services/tag-service';
import { UserTagService } from '../services/user-tag-service';

@inject(TagService, UserTagService, EventAggregator)
export class TagManager {

  @bindable() users = [];
  hasResults = false;
  tagSelected = null;

  constructor(tagService, userTagService, eventAggregator) {
    this.tagService = tagService;
    this.userTagService = userTagService;
    this.eventAggregator = eventAggregator;
    this.userTagService.setViewModel(this);
  }

  setUsers(usersFound) {
    this.users = usersFound;
  }

  findUsersByTag(tag) {
    this.hasResults = true;
    this.tagSelected = tag;
    this.userTagService.findUsersByTagName(tag.name);
  }

  deleteTag() {
    return this.tagService.deleteTag(this.tagSelected.id)
      .then(() => {
        this.eventAggregator.publish('tag-deleted', this.tagSelected);
        this.tagSelected = null;
      });
  }

}
