import { load } from 'aurelia-environment';
import { LogManager } from 'aurelia-framework';
import { ConsoleAppender } from 'aurelia-logging-console';
import 'materialize';
import translationConfig from './configurations/translation-config';

export function configure(aurelia) {
  // We do not need to log everything, just errors.
  LogManager.setLevel(LogManager.logLevel.error);
  LogManager.addAppender(new ConsoleAppender());

  load().then(() => {
    aurelia.use
    .standardConfiguration()
    .plugin('aurelia-materialize-bridge', bridge => bridge.useAll())
    .plugin('aurelia-auth')
    .plugin('aurelia-i18n', translationConfig(aurelia))
    .feature('resources/custom-elements')
    .feature('resources/value-converters');

    aurelia.start().then(a => a.setRoot());
  });
}
