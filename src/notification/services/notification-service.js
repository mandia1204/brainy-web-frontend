import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { NotificationClient } from '../../clients/notification-client';

@inject(BaseComponent, NotificationClient)
export class NotificationService {

  constructor(baseComponent, notificationClient) {
    this.baseComponent = baseComponent;
    this.notificationClient = notificationClient;
    this.logger = LogManager.getLogger(NotificationService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  retrieveNotifications() {
    this.baseComponent.showProgressHub();

    return this.notificationClient.getNotifications()
      .then((response) => {
        this.viewModel.setNotifications(response);
        return response;
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
        return error;
      })
      .then((response) => {
        this.baseComponent.dismissProgressHub();
        return response;
      });
  }

}

