import { inject } from 'aurelia-framework';
import { Redirect } from 'aurelia-router';
import { AuthService } from 'aurelia-auth';
import localStorageManager from './local-storage-manager';
import ApplicationParameter from '../shared/application-parameter';

@inject(AuthService, ApplicationParameter)
export class AuthorizeUserRegisteredStep {

  principalFirstLoginRoute = 'myprofile';
  homeRoute = 'home';
  isLoggedIn;
  isFirstLogin;
  isAdmin;

  constructor(auth, applicationParameter) {
    this.auth = auth;
    this.applicationParameter = applicationParameter;
  }

  run(routingContext, next) {
    this.updateAttributes();

    if (this.isLoggedInAndAccessToPageNotAllowedToUserFirstLogin(routingContext)) {
      return next.cancel(new Redirect(this.principalFirstLoginRoute));
    }

    if (routingContext.config.admin && !this.applicationParameter.computeIsAdmin()) {
      return next.cancel(new Redirect(this.homeRoute));
    }

    return next();
  }

  isLoggedInAndAccessToPageNotAllowedToUserFirstLogin(routingContext) {
    return this.isLoggedIn
        && this.isFirstLogin
        && this.isAdmin
        && routingContext.getAllInstructions().some(i => !i.config.authFirstLogin);
  }

  updateAttributes() {
    this.isLoggedIn = this.auth.isAuthenticated();
    this.isFirstLogin = localStorageManager.isFirstLogin();
    this.isAdmin = this.applicationParameter.isAdmin;
  }
}
