/* global localStorage */

import { constants } from './constants';

class LocalStorageManager {

  isFirstLogin() {
    return localStorage.getItem(constants.FIRST_LOGIN) === 'true';
  }

  getEmailUser() {
    const token = this.getAuthToken();
    if (!token) {
      return false;
    }
    // eslint-disable-next-line no-undef
    const jsonWebToken = jwt_decode(token);
    return jsonWebToken.unique_name;
  }

  getAuthToken() {
    return localStorage.getItem(constants.AURELIA_TOKEN);
  }

  setAuthToken(token) {
    localStorage.setItem(constants.AURELIA_TOKEN, token);
  }

  setFirstLogin(value) {
    localStorage.setItem(constants.FIRST_LOGIN, value);
  }

  removeAuthToken() {
    localStorage.removeItem(constants.AURELIA_TOKEN);
  }

  getJsonWebTokenBody() {
    const jwt = this.getAuthToken();
    // eslint-disable-next-line no-undef
    return jwt_decode(jwt);
  }

  isCurrentUser(email) {
    const jsonWebToken = this.getJsonWebTokenBody();
    return email === jsonWebToken.unique_name;
  }
}

const localStorageManager = new LocalStorageManager();

export default localStorageManager;
