export default class HtmlToExcelExport {
  uri = 'data:application/vnd.ms-excel;base64,';
  template= '<html xmlns:o="urn:schemas-microsoft-com:office:office"' +
  'xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
  '<head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name>' +
 '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
 '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';

  base64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  format(str, ctx) {
    return str.replace(/{(\w+)}/g, (m, p) => ctx[p]);
  }

  export(tableId, name) {
    const table = document.getElementById(tableId);
    const ctx = { worksheet: name || 'Worksheet', table: table.innerHTML };
    window.location.href = this.uri + this.base64(this.format(this.template, ctx));
  }
}
