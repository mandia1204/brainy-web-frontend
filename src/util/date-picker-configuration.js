export default {
  advancedOptions: {
    closeOnSelect: true,
    closeOnClear: true,
    max: new Date(),
    selectYears: 50,
  },
};
