export default class Confirmation {

  constructor(attributes = {}) {
    this.status = attributes.status || null;
    this.messageEmail = attributes.messageEmail || '';
  }
}
