export default class Parameter {

  constructor(attributes = {}) {
    this.code = attributes.code || null;
    this.name = attributes.name || '';
    this.active = attributes.active || false;
    this.parameterProperty = attributes.parameterProperty || { position: {}, location: {} };
    // this.headquarters
  }
}
