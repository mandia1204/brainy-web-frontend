import { inject } from 'aurelia-framework';
import { HomeService } from '../services/home-service';
import { constants } from '../../util/constants';

@inject(HomeService)
export class Home {

  hasResults = false;
  notificationAllowed = constants.NOTIFICATION_ALLOWED;
  skill = {};

  constructor(homeService) {
    this.homeService = homeService;
    this.homeService.setViewModel(this);
  }

  setResults(results) {
    const total = results.length;
    this.results = results;
    this.hasResults = total > 0;
  }

  doSearch(query) {
    this.homeService.searchFromAPI(query);
  }
}
