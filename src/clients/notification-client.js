import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class NotificationClient {

  constructor(client) {
    this.client = client;
  }

  getNotifications() {
    return this.client.getFrom('/notificationApi/notifications')
        .then(response => response.json())
        .then(jsonResponse => jsonResponse.elements);
  }

}

