import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class BadgeClient {

  constructor(client) {
    this.client = client;
  }

  getBadges(username) {
    return this.client.getFrom(`/badgeApi/profile/${username}/badges`)
         .then(response => response.json())
         .then(jsonResponse => jsonResponse.elements);
  }
}
