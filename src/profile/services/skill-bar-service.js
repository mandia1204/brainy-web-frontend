import { inject } from 'aurelia-framework';
import { Endorsement } from '../../models/endorsement';
import { EndorsementClient } from '../../clients/endorsement-client';

@inject(EndorsementClient)
export class SkillBarService {

  constructor(endorsementClient) {
    this.endorsementClient = endorsementClient;
  }

  makeSkillEndorsement(email, skill) {
    return this.endorsementClient
        .updateSkillEndorsement(email, skill.id, new Endorsement({
          value: skill.guestLevel,
        }));
  }

  deleteSkillEndorsement(email, skill) {
    return this.endorsementClient
         .deleteSkillEndorsement(email, skill.id);
  }
}
