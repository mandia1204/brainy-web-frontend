/* global Element, $ */

import { bindable, inject } from 'aurelia-framework';
import { _ } from 'lodash';
import { AutocompleteSkillsService } from '../../services/autocomplete-skills-service';
import { constants } from '../../../util/constants';

@inject(Element, AutocompleteSkillsService)
export class AutocompleteSkills {
  static MIN_LENGTH =2;
  static KEY_CODES_NOT_ALLOWED = [
    constants.keyCodes.ENTER,
    constants.keyCodes.ESCAPE,
    constants.keyCodes.END,
    constants.keyCodes.HOME,
    constants.keyCodes.LEFT_ARROW,
    constants.keyCodes.UP_ARROW,
    constants.keyCodes.RIGHT_ARROW,
    constants.keyCodes.DOWN_ARROW,
    constants.keyCodes.SHIFT,
  ];

  @bindable() user;
  @bindable() values = [];
  @bindable() skill;
  @bindable() filterAllowed = true;
  input;
  @bindable() updateRate;
  tabPressed = false;

  constructor(element, skillService) {
    this.element = element;
    this.skillService = skillService;
  }

  attached() {
    this.initializeInputEvents();
    this.initializeEventMousedown();
  }

  initializeInputEvents() {
    $(this.element).on('keyup', 'input', _.debounce(this.processEventKeyUpDebounce(), 200));
    $(this.element).on('keydown', 'input', this.processEventKeyDown.bind(this));
  }

  processEventKeyUpDebounce() {
    return event => this.processEventKeyUp(event);
  }

  processEventKeyUp(event) {
    this.input = $(this.element).find('input');
    if (this.isValueInputLessThanTheMinumunLength()) {
      this.values = [];
    } else if (this.isKeyCodeAllowed(event) && !this.tabPressed) {
      this.searchSkills();
    }
    this.tabPressed = false;
  }

  isKeyCodeAllowed(event) {
    return !AutocompleteSkills.KEY_CODES_NOT_ALLOWED
                .find(element => element === event.which);
  }

  searchSkills() {
    this.skillService.searchSkills(this.input.val())
      .then((response) => {
        if (this.isFocusInput()) {
          this.processResponseSearchSkills(response);
        }
      });
  }

  processResponseSearchSkills(response) {
    if (this.isValueInputLessThanTheMinumunLength()) {
      this.values = [];
    } else if (this.filterAllowed) {
      const newValues = this.extractValues(response);
      this.filterValues(newValues);
    } else {
      this.values = this.extractValues(response);
    }
  }

  extractValues(response) {
    return response.map(element => element.name);
  }

  filterValues(newValues) {
    this.values = newValues.filter(element =>
                  !this.user.skillList.some(skill => skill.name === element));
  }

  initializeEventMousedown() {
    // eslint-disable-next-line no-undef
    $(document).on('mousedown', (event) => {
      if (this.notIsInputOrSelectOfAutocompleteChips($(event.target))) {
        this.values = [];
      }
    });
  }

  notIsInputOrSelectOfAutocompleteChips(element) {
    return !this.isInputOfAutocompleteChips(element) &&
           !this.isSelectOfAutocompleteChips(element);
  }

  isInputOfAutocompleteChips(element) {
    return element.is('input') && element.closest('autocomplete-skills');
  }

  isSelectOfAutocompleteChips(element) {
    return element.is('span') && element.closest('autocomplete-skills');
  }

  selectValue() {
    return (value) => {
      $(this.input).val(value);
      this.values = [];
      this.skill.name = value;
    };
  }

  isValueInputLessThanTheMinumunLength() {
    return this.input.val().trim().length < AutocompleteSkills.MIN_LENGTH;
  }

  isFocusInput() {
    return this.input.is(':focus');
  }

  processEventKeyDown(event) {
    const ev = event.originalEvent;

    if (ev.shiftKey) {
      const acceptedCodes = Array(5).fill().map((x, i) => i + 1).reduce((a, b) => a.concat([`Digit${b}`, `Numpad${b}`]), []);

      if (acceptedCodes.indexOf(ev.code) !== -1) {
        const rateinput = ev.code.replace('Digit', '').replace('Numpad', '');
        if (this.updateRate) {
          this.updateRate({ rate: rateinput });
          return false;
        }
      }
    } else if (ev.code === 'Tab') {
      this.selectOnTab();
      return false;
    }
    return true;
  }

  selectOnTab() {
    if (this.values.length > 0) {
      this.skill.name = this.values[0];
      this.values = [];
      this.tabPressed = true;
    }
  }
}
