import { bindable, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../../util/base-component';

export class SkillRating extends BaseComponent {

  @bindable() user;
  @bindable() updateProperty;
  @bindable() remove;

  constructor() {
    super();
    this.logger = LogManager.getLogger(SkillRating.name);
  }

  removeItem(skill) {
    if (this.remove && typeof this.remove === 'function') {
      this.remove(skill);
    }
  }
}
