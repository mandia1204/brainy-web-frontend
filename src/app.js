import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { FetchConfig } from 'aurelia-auth';
import { BrainyWebRouter } from './routing/brainy-web-router';

@inject(EventAggregator, FetchConfig, BrainyWebRouter)
export class App {
  spinnerAllowed = false;
  subscribers = [];
  mainStyle = '';

  constructor(eventAggregator, fetchConfig, brainyWebRouter) {
    this.ea = eventAggregator;
    this.fetchConfig = fetchConfig;
    this.brainyWebRouter = brainyWebRouter;
  }

  activate() {
    this.fetchConfig.configure();
    this.brainyWebRouter.configure();
  }

  attached() {
    const events = [
      'showProgressHub', 'dismissProgressHub',
      'enterLandingPage', 'leaveLandingPage',
    ];

    events.forEach((eventName) => {
      this.subscribers
        .push(this.ea.subscribe(eventName, () => this[eventName]()));
    });
  }

  detached() {
    this.subscribers.forEach(es => es.dispose());
  }

  showProgressHub() {
    this.spinnerAllowed = true;
  }

  dismissProgressHub() {
    this.spinnerAllowed = false;
  }

  enterLandingPage() {
    this.mainStyle = 'landingPage';
  }

  leaveLandingPage() {
    this.mainStyle = '';
  }

}
