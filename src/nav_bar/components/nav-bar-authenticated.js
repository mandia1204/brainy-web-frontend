import { bindable } from 'aurelia-framework';

export class NavBarAuthenticated {
  @bindable changeLanguage;
  @bindable isFirstLogin;
  @bindable isAdmin;
  @bindable singleHeadquarter;
  @bindable headquarters;
  @bindable selectedHeadquarter;
  @bindable notificationAllowed;
  @bindable logout;
  @bindable changeHeadquarter;

  change(language) {
    this.changeLanguage(language);
  }

  logoutAction() {
    this.logout();
  }

  changeHead(value) {
    this.changeHeadquarter({ headquarter: value });
  }
}
